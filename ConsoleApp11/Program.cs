﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp11
{
    class Program
    {
        static void Main(string[] args)
        {

            string failinimi = @"..\..\b.txt";

            var loetud = File.ReadAllText(failinimi); //loeb teksti failist
            Console.WriteLine(loetud);

            var loetudRead = File.ReadAllLines(failinimi); //loeb read
            for (int i = 0; i < loetudRead.Length; i++)
            {
                Console.WriteLine($"rida {i} -- {loetudRead[i]}");
            }
        }
    }
}
